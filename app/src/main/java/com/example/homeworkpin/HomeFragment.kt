package com.example.homeworkpin

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.homeworkpin.databinding.FragmentHomeBinding


class HomeFragment : BaseFragment<FragmentHomeBinding>(
FragmentHomeBinding::inflate
) {

    lateinit var cusAdapter: CusAdapter
    var PINCODE = "0934"
    override fun start() {

        val list = mutableListOf<Int>(1,2,3,4,5,6,7,8,9, 10, 0, 12)

        cusAdapter = CusAdapter(list)
        binding.recyclerview.adapter = cusAdapter
        binding.recyclerview.layoutManager = GridLayoutManager(requireContext(), 3)

        cusAdapter.setOnItemClickListener(object : CusAdapter.onItemClickListener {
            var pin = ""
            var total:Int = 0
            override fun onItemClick(position: Int) {

                if(position != 9 && position != 11) {
                    val curItem: Int = list[position]
                    pin += curItem
                    total++
                }

                if(position == 11){
                    pin = pin.dropLast(1)
                    total--
                    when(total) {
                        0 -> {
                            binding.view.setBackgroundResource(R.drawable.ic_ellipse_44)
                            binding . view2 . setBackgroundResource (R.drawable.ic_ellipse_44)
                            binding . view3 . setBackgroundResource (R.drawable.ic_ellipse_44)
                            binding . view4 . setBackgroundResource (R.drawable.ic_ellipse_44)
                        }
                      1 ->  {
                          binding . view2 . setBackgroundResource (R.drawable.ic_ellipse_44)
                          binding . view3 . setBackgroundResource (R.drawable.ic_ellipse_44)
                          binding . view4 . setBackgroundResource (R.drawable.ic_ellipse_44)
                      }
                        2 ->  {

                            binding.view3.setBackgroundResource (R.drawable.ic_ellipse_44)
                            binding.view4.setBackgroundResource (R.drawable.ic_ellipse_44)
                        }
                        3->  binding.view4.setBackgroundResource (R.drawable.ic_ellipse_44)
                    }

                }




                when(total){
                    1 -> binding.view.setBackgroundColor(Color.GREEN)
                    2 -> binding.view2.setBackgroundColor(Color.GREEN)
                    3 -> binding.view3.setBackgroundColor(Color.GREEN)
                    4 -> binding.view4.setBackgroundColor(Color.GREEN)

                }
                if(total == 4 && pin == PINCODE){
                    pin = ""
                    Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()

                    binding.view.setBackgroundResource(R.drawable.ic_ellipse_44)
                    binding.view2.setBackgroundResource(R.drawable.ic_ellipse_44)
                    binding.view3.setBackgroundResource(R.drawable.ic_ellipse_44)
                    binding.view4.setBackgroundResource(R.drawable.ic_ellipse_44)
                    total = 0

                } else if(total == 4 && pin != PINCODE){
                    Toast.makeText(requireContext(), "Failure", Toast.LENGTH_SHORT).show()
                    pin = ""
                    binding.view.setBackgroundResource(R.drawable.ic_ellipse_44)
                    binding.view2.setBackgroundResource(R.drawable.ic_ellipse_44)
                    binding.view3.setBackgroundResource(R.drawable.ic_ellipse_44)
                    binding.view4.setBackgroundResource(R.drawable.ic_ellipse_44)
                    total = 0
                }
            }
        })

    }
}