package com.example.homeworkpin

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.homeworkpin.databinding.ItemBinding

class CusAdapter(private val data:MutableList<Int>) :
    RecyclerView.Adapter<CusAdapter.ViewHolder>() {

    private lateinit var mListener: onItemClickListener

    interface onItemClickListener {
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener) {
        mListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false), mListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder(private val binding: ItemBinding, private val listener: onItemClickListener): RecyclerView.ViewHolder(binding.root){

        fun onBind(position: Int){
            bindUI()
        }

        fun bindUI(){
            val currData = data[adapterPosition]
            if(currData == 10){
                binding.ivItem.setImageResource(R.drawable.ic_touch_id)
            } else if(currData == 12){
                binding.ivItem.setImageResource(R.drawable.ic_backspace)
            }
            when(currData){
                10 -> binding.itemText.text = ""
                12 -> binding.itemText.text = ""
                else -> binding.itemText.text = currData.toString()
            }

            binding.ivItem.setOnClickListener {
                    listener.onItemClick(adapterPosition)
            }
        }
    }

}